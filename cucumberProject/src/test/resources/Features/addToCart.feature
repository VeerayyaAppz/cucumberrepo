Feature: Feature to add items to Amazon cart 

@VerifyCartItems 
Scenario: Check selected items are added to the cart successfuly 

	Given User is on Amazon landing page 
	When User clicks Electronics category 
	And User scrolls down till Samsung Brand 
	And User clicks on Samsung Brand 
	And User clicks on first item displayed 
	And User clicks on add to cart 
	And User clicks on cart instead on checkout 
	And User closes the Shopping Cart screen 
	And User clicks on cart container 
	Then User is displayed Shopping cart screen 
	And User verify the number of items added to cart