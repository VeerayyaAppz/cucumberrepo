package StepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddToCartSteps {
	WebDriver driver;
	String winBefore;
	@Given("User is on Amazon landing page")
	public void user_is_on_amazon_landing_page() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.amazon.in/");
	}

	@When("User clicks Electronics category")
	public void user_clicks_electronics_category() {
		driver.findElement(By.xpath("//*[@id=\"nav-xshop\"]/a[5]")).click();
	}

	@And("User scrolls down till Samsung Brand")
	public void user_scrolls_down_till_samsung_brand() {
		WebElement target = driver.findElement(By.xpath("(//*[@class='a-size-base a-color-base'])[5]"));// [6]Samsung
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", target);
	}

	@And("User clicks on Samsung Brand")
	public void user_clicks_on_samsung_brand() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("(//*[@class='a-size-base a-color-base'])[5]")).click();
	}

	@And("User clicks on first item displayed")
	public void user_clicks_on_first_item_displayed() {
		winBefore = driver.getWindowHandle();
		driver.findElement(By.xpath("(//img[@class='s-image'])[1]")).click();
	}

	@And("User clicks on add to cart")
	public void user_clicks_on_add_to_cart() {
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@id='add-to-cart-button']")).click();
	}

	@And("User clicks on cart instead on checkout")
	public void user_clicks_on_cart_instead_on_checkout() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			driver.findElement(By.xpath("//*[@id=\'attach-sidesheet-view-cart-button\']/span/input")).click();
		} catch (NoSuchElementException e) {
			driver.findElement(By.xpath("//*[@id=\'hlb-view-cart-announce\']")).click();;
		}
	}

	@And("User closes the Shopping Cart screen")
	public void user_closes_the_shopping_cart_screen() {
		driver.close();
		
	}

	@And("User clicks on cart container")
	public void user_clicks_on_cart_container() {
		driver.switchTo().window(winBefore);
		driver.findElement(By.id("nav-cart-text-container")).click();
	}

	@Then("User is displayed Shopping cart screen")
	public void user_is_displayed_shopping_cart_screen() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Then("User verify the number of items added to cart")
	public void user_verify_the_number_of_items_added_to_cart() throws InterruptedException {
		int cartItems = driver.findElements(By.xpath("//*[@class='a-truncate-cut']")).size();
		if (cartItems != 0) {
			if (cartItems >= 2) {
				System.out.println("Selected " + cartItems + " are added to the cart succesfully!");

			} else {
				System.out.println("Selected " + cartItems + " item is added to the cart succesfully!");
			}

		} else {
			System.out.println("The cart is empty!!");
		}
		Thread.sleep(5000);
		driver.quit();
	}

}
